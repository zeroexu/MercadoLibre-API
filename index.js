const express = require('express');
const config = require('./src/server/config');
const { MESSAGES } = require('./src/utils/messages');
const app = config(express());

// Starting the server
app.listen(app.get('port'), () => {
  console.log(MESSAGES.SETTINGS.PORT, app.get('port'));
});
