#Rest API de Test para MercadoLibre


Descripcion
----------------------------
Esta construido con NodeJs + Express

Responde dos servicios, el primero es la busqueda de una palabra clave a la API, provista por mercadolibre (para pruebas)
el segundo es que arroja la descripcion de un producto en especifico por ID


Requerimientos
---------------------------

- Node 12.12.+
- Npm 6.9.0

IMPORTANTE:
--------------------------
Este proyecto interactua con la webUI que esta realizada con NextJs (ReactJs)
que lo puedes conseguir acá

- https://gitlab.com/zeroexu/mercadolibre

Instalacion
---------------------------

Para instalar solo tienes que ejecutar en el terminal

- npm install
- npm run start 
- npm run dev para el modo desarrollo

En el modo desarrollo el App trabaja por el puerto 5001
En el modo produccion el App trabaja por el puerto 3001

Si deseas puedes configurar tus variables de entorno en al carpeta env
Puedes integrar estos archivos a tu archivo de configuracion de integracion continua para que hagas deployment a tu servidor.


