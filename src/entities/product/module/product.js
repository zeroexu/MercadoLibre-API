const { requestSearch, requestItem, requestItemDescription } = require('../../../utils/utils')
const { AUTHOR } = require('../../../server/keys');

const search = (search) => {
  return requestSearch(search).then(response => {
    return refactorSearchResponse(response)
  })
};

const detail = (id) => {
  const promises = []
  promises.push(requestItem(id))
  promises.push(requestItemDescription(id))
  return Promise.all(promises).then(response => {
    return {
      author: setAuthor(),
      item: {
        id: response[0].id,

        title: response[0].title,
        price: {
          currency: response[0].currency_id,
          amount: response[0].price,
          decimals: 0,
        },
        picture: response[0].thumbnail,
        condition: response[0].condition,
        free_shipping: response[0].shipping.free_shipping,
        sold_quantity: response[0].sold_quantity,
        description: response[1].plain_text
      }
    }
  })
};

const setAuthor = () => {
  return {
    name: AUTHOR.FIRSTNAME,
    lastname: AUTHOR.LASTNAME
  }
}

const refactorSearchResponse = (originalResponse) => {
  const { results, filters } = originalResponse
  return {
    author: setAuthor(),
    categories: getCategoriesFromFilters(filters),
    items: getItemsFromResults(results)
  }
}

const getCategoriesFromFilters = (filters = []) => {
  try {
    if (filters.length > 0 && filters[0].values && filters[0].values.length > 0 && filters[0].values[0].path_from_root) {
      const path = filters[0].values[0].path_from_root
      let categories = []
      path.forEach(element => { return categories.push(element.name) })
      return categories
    }
    return []
  } catch (e) { console.log(e) }
  return []
}

const getItemsFromResults = (results = []) => {
  let items = []
  results.forEach(item => {
    items.push({
      id: item.id,
      title: item.title,
      price: {
        currency: item.currency_id,
        amount: item.price,
        decimals: 00
      },
      picture: item.thumbnail,
      condition: item.condition,
      free_shipping: item.shipping.free_shipping,
      address: item.address
    })
  })
  return items;
}

module.exports = {
  search: search,
  detail: detail,
};
