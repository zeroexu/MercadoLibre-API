const product_module = require('../module/product');
const { search, detail } = product_module
const result = require('../../../utils/result');



const searchProduct = (req, res) => {
  search(req.params.search)
    .then((data) => {
      result.ok(res, data);
    }).catch(e => {
      result.error(req, res, e)
    });
};

const detailProduct = (req, res) => {
  detail(req.params.id)
    .then((data) => {
      result.ok(res, data);
    }).catch(e => {
      result.error(req, res, e)
    });
};


module.exports = {
  searchProduct: searchProduct,
  detailProduct: detailProduct,
};
