const express = require('express');
const { MESSAGES } = require('../utils/messages');
const {
  searchProduct,
  detailProduct,
} = require('../entities/product/controller/product');



const router = express.Router();

module.exports = app => {
  router.get('/', (req, res) => {
    res.end(MESSAGES.STATUS_RESPONSE.OK);
  });
  //------------------------------------ Products   
  router.get('/product/:id', detailProduct);
  router.get('/product/search/:search', searchProduct);

  router.get('/*', (req, res) => {
    res.status(404).json({ 'status': 'fail', 'data': {} });
  });
  app.use(router);
};
