const ok = (res, data) => {
  return res.status(200).json(data);
};

const badData = (res, data) => {
  const result = {
    result: 'BAD_DATA',
    data: data
  };
  return res.status(409).json(result);
};

const notFound = (res, message) => {
  const result = {
    result: 'NOT_FOUND',
    data: { message: message }
  };
  return res.status(404).json(result);
};

const badRequest = (res, data) => {
  const result = {
    result: 'BAD_REQUEST',
    data: data
  };
  return res.status(400).json(result);
};

const error = (req, res, data) => {

  const result = {
    result: 'ERROR',
    data: data
  };
  console.log(result)
  return res.status(500).json(result);
};

module.exports = {
  ok: ok,
  error: error,
  badRequest: badRequest,
  badData: badData,
  notFound: notFound
};
