const requestPromise = require('request-promise');

const request = (url) => {
  var options = {
    uri: url,
    headers: {
      'User-Agent': 'Request-Promise'
    },
    method: 'GET',
    json: true // Automatically parses the JSON string in the response
  };
  return requestPromise(options);
}

const requestSearch = (search) => {
  return request('https://api.mercadolibre.com/sites/MLA/search?q=' + search);
}

const requestItem = (id) => {
  return request('https://api.mercadolibre.com/items/' + id);
}

const requestItemDescription = (id) => {
  return request('https://api.mercadolibre.com/items/' + id + '/description');
}

module.exports = {
  requestSearch: requestSearch,
  requestItem: requestItem,
  requestItemDescription: requestItemDescription
};
