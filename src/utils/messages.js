module.exports = {
  MESSAGES: {
    DATABASE: {
      CONNECTION: {
        SUCCESS: 'La base de datos està conectada',
        FAIL: 'Ha fallado el proceso de conexion a la base de datos'
      }
    },
    TEST: 'Hola Mundo',
    STATUS_RESPONSE: {
      FAIL: 'FAIL',
      SUCCESS: 'SUCCESS',
      FORBIDDEN: 'FORBIDDEN',
      OK: 'ok'
    },
    SETTINGS: {
      PORT: 'APP esta corriendo en el puerto:',
      NAME: 'La Aplicacion se llama:',
      ENV: 'El entorno de ejecucion es:'
    }
  }
};
