const path = require('path');
const morgan = require('morgan');
const express = require('express');
const cors = require('cors');
const routes = require('../routes/routes');
const result = require('../utils/result');
const { SETTINGS, APP_KEYS } = require('./keys');

module.exports = app => {
  // Settings
  app.set(APP_KEYS.PORT, SETTINGS.PORT);
  app.set(APP_KEYS.ENV, SETTINGS.NODE_ENV);
  app.set(APP_KEYS.APP_NAME, SETTINGS.APP_NAME);

  // middlewares
  app.use(morgan('dev'));
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());


  // Routes
  routes(app);

  // Static files
  app.use('/public', express.static(path.join(__dirname, '../public')));

  //Set CORS
  const corsOptions = {
    origin: APP_KEYS.CORS_HOST,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }
  app.use(cors(corsOptions))

  // Error Handling
  if ('development' === app.get(APP_KEYS.ENV)) {
    console.log('Deteccion de errores habilitada');
    //app.use(errorHandler());
  }

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use((err, req, res) => {
    return result.error(req, res, err.message);
  });

  return app;
};
