require('dotenv').config({ path: process.env.NODE_ENV ? `${__dirname}/../../env/.env.${process.env.NODE_ENV}` : `${__dirname}/../../env/.env.development` });

console.log('process.env.NODE_ENV', process.env.NODE_ENV);
module.exports = {
  SETTINGS: {
    PORT: process.env.PORT,
    APP_NAME: process.env.APP_NAME,
    NODE_ENV: process.env.NODE_ENV,
    MAINTENANCE: process.env.MAINTENANCE,
    CORS_HOST: process.env.EXEPTIONS_HOST
  },
  AUTHOR: {
    FIRSTNAME: process.env.AUTHOR_FIRSTNAME,
    LASTNAME: process.env.AUTHOR_LASTNAME
  },
  MERCADO_LIBRE: {
    search: `${process.env.MERCADOLIBRE_API_ENDPOINT}/sites/MLA/search?q=​`,
    item: `${process.env.MERCADOLIBRE_API_ENDPOINT}/items/​:id`
  },
  APP_KEYS: {
    PORT: 'port',
    ENV: 'env',
    APP_NAME: 'app_name'
  }
};
